/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author M
 */
public class MySQLConnection {

    private static String DATABASE_USER_NAME;
    private static String DATABASE_USER_PASSWORD;
    private static String DATABASE_URL;

    public MySQLConnection(String DATABASE_URL, String DATABASE_USER_NAME, String DATABASE_USER_PASSWORD) {
        MySQLConnection.DATABASE_URL = DATABASE_URL;
        MySQLConnection.DATABASE_USER_NAME = DATABASE_USER_NAME;
        MySQLConnection.DATABASE_USER_PASSWORD = DATABASE_USER_PASSWORD;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

    }

//    mySQLConnection(String url, String user, String password) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public static Connection ConnectToDatabase() {
        Connection con = null;
        try {
            con = DriverManager.getConnection(DATABASE_URL, DATABASE_USER_NAME, DATABASE_USER_PASSWORD);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return con;
    }

    public void Query(String SQL) {

        try {
            Statement st = ConnectToDatabase().createStatement();
            ResultSet rset = st.executeQuery(SQL);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
}
