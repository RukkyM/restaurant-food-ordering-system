/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Table;

/**
 *
 * @author M
 */
public class Table {
    private String id;
    private String table_capacity;

    public Table(String id, String table_capacity) {
        this.id = id;
        this.table_capacity = table_capacity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTable_capacity() {
        return table_capacity;
    }

    public void setTable_capacity(String table_capacity) {
        this.table_capacity = table_capacity;
    }
}
