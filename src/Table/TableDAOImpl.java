/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Table;

import connect.MySQLConnection;
import connect.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author M
 */
public class TableDAOImpl implements TableDAO{
 MySQLConnection mysql = new MySQLConnection(Util.SALES_APP_DB_URL, Util.SALES_APP_DB_USER, Util.SALES_APP_DB_PASSWORD);
  Connection con = MySQLConnection.ConnectToDatabase();
    @Override
    public void Create(Table obj) {
        try {
        PreparedStatement pre = con.prepareStatement("INSERT INTO Table_master (id,table_capacity)VALUES (?,?)");
       
        pre.setString(1, obj.getId());
        pre.setString(2, obj.getTable_capacity());
       
        pre.executeUpdate();
         
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
    }
    
}
