/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.login.Department;
import connect.MySQLConnection;
import connect.Util;
import departments.DepartmentDAO;
import departments.DepartmentDAOImpl;
import java.io.DataInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author M
 */
public class ManageInput {
    Scanner scan = new Scanner(System.in);
    DepartmentDAO dp = new DepartmentDAOImpl();
    Department d = new Department();
    MySQLConnection mysql = new MySQLConnection(Util.SALES_APP_DB_URL, Util.SALES_APP_DB_USER, Util.SALES_APP_DB_PASSWORD);
   Connection con = MySQLConnection.ConnectToDatabase();
   DataInputStream dis = new DataInputStream(System.in);
   public void addDepartment(String id, String name ){
       try{
            PreparedStatement pre = con.prepareStatement("INSERT INTO department (id,name)VALUES (?,?)");
       
        pre.setString(1, id);
        pre.setString(2, name);
        pre.executeUpdate();
         
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
       }
   public void addEmployee(String id, String name, String Department_Id){
       try{
             PreparedStatement pre = con.prepareStatement("INSERT INTO Employee VALUES(?,?,?)");
            pre.setString(1, id);
            pre.setString(2, name);
            pre.setString(3, Department_Id);
            
            pre.executeUpdate();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
       }
   public void addTable(String id, String table_capacity){
         try {
        PreparedStatement pre = con.prepareStatement("INSERT INTO Table_master (id,table_capacity)VALUES (?,?)");
       
        pre.setString(1, id);
        pre.setString(2, table_capacity);
       
        pre.executeUpdate();
         
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
   }
   }
   

