/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import connect.MySQLConnection;
import connect.Util;
import departments.Department;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author M
 */
public class EmployeeDAOImpl implements EmployeeDAO{
 MySQLConnection mysql = new MySQLConnection(Util.SALES_APP_DB_URL, Util.SALES_APP_DB_USER, Util.SALES_APP_DB_PASSWORD);
  Connection con = MySQLConnection.ConnectToDatabase();
    @Override
    public void Create(Employee obj) {
        try{
            PreparedStatement pre = con.prepareStatement("INSERT INTO Employee VALUES(?,?,?)");
            pre.setString(1, obj.getId());
            pre.setString(2, obj.getName());
            pre.setString(3, obj.getDepartment_Id());
            
            pre.executeUpdate();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public Employee find(String id) {
          Employee depart = null;
    try {
        PreparedStatement pre = con.prepareStatement("SELECT * FROM Employee WHERE id = ?");
        ResultSet result = pre.executeQuery();
        
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    return depart;
    }

    @Override
    public void Update(Employee obj) {
         try {
        PreparedStatement pre = con.prepareStatement("UPDATE Employee SET name = ? WHERE id = ?");
      
        pre.setString(1, obj.getName());
        pre.executeUpdate();
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    }

    @Override
    public void delete(String id) {
         try {
        PreparedStatement pre = con.prepareStatement("DELETE FROM Employee WHERE id = ?");
        pre.setString(1,id);
        pre.executeUpdate();
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
    }
    
}
