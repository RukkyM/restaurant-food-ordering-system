/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;


import java.util.List;

/**
 *
 * @author M
 */
public interface EmployeeDAO {
    public void Create (Employee obj);
    public Employee find (String id);
    public void Update(Employee obj);
    public void delete (String  id);
}
