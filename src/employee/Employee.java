/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author M
 */
public class Employee {
    private String id;
    private String name;
    private String Department_Id;
    public Employee(String id, String name, String Department_Id) {
        this.id = id;
        this.name = name;
        this.Department_Id = Department_Id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment_Id() {
        return Department_Id;
    }

    public void setDepartment_Id(String Department_Id) {
        this.Department_Id = Department_Id;
    }

    
}
