package departments;

import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author M
 */
public interface DepartmentDAO {
    public void Create (Department obj);
    public Department find (String id);
    public void Update(Department obj);
    public void delete (String id);
}
