package departments;

import connect.MySQLConnection;
import connect.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author M
 */
public class DepartmentDAOImpl implements DepartmentDAO {
  MySQLConnection mysql = new MySQLConnection(Util.SALES_APP_DB_URL, Util.SALES_APP_DB_USER, Util.SALES_APP_DB_PASSWORD);
  Connection con = MySQLConnection.ConnectToDatabase();
    @Override
    public void Create(Department obj) {
         try {
        PreparedStatement pre = con.prepareStatement("INSERT INTO Department (id,name)VALUES (?,?)");
       
        pre.setString(1, obj.getId());
        pre.setString(2, obj.getName());
       
        pre.executeUpdate();
         
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
    }

    @Override
    public Department find(String id) {
           Department depart = null;
    try {
        PreparedStatement pre = con.prepareStatement("SELECT * FROM Department WHERE id = ?");
        ResultSet result = pre.executeQuery();
        
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    return depart;
    }

   
    @Override
    public void Update(Department obj) {
         try {
        PreparedStatement pre = con.prepareStatement("UPDATE department SET name = ? WHERE id = ?");
      
        pre.setString(1, obj.getName());
        pre.executeUpdate();
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    }

    @Override
    public void delete(String id) {
        try {
        PreparedStatement pre = con.prepareStatement("DELETE FROM Department WHERE id = ?");
        pre.setString(1,id);
        pre.executeUpdate();
    } catch (SQLException ex) {
       ex.printStackTrace();
    }
    }
    
}
